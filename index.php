<?php

// http://php.net/manual/en/pdo.construct.php

/* Connect to a MySQL database using driver invocation */
$dsn = 'mysql:dbname=joomla;host=mysql';
$user = 'root';
$password = 'joomla';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}

echo PHP_EOL;
